# Firekraken - Functions

FireKraken (mix between Firebase and Kraken) is a dashboard to follow your cryptocurrency investment made on Kraken.

# How does it work ?

Every 30 minutes, a Firebase function is run. This function will fetch the latest data from your Kraken account regarding your balance and the value of the coins you own.
Those data will be added into a Firestore database after some processing (computing total values, highest value, ..).

This project is only the server code, which fetches and saves data. The frontend project can be found here : [FireKraken/Hosting](https://gitlab.com/firekraken/hosting)

# How do I use it ?

1. Create an API key from your Kraken account
1. Set the two environment variables : `KRAKEN_API_KEY` and `KRAKEN_API_SECRET_KEY`
1. Setup your Firebase account
1. Create the project you will use
    1. Activate Functions (you will need the Blaze pay-as-you-go plan)
    1. Activate Hosting (if you use the frontend project)
    1. Activate Firestore
1. Update `.firebaserc` with your new project
1. Install the dependencies from the `function` folder : `npm i``
1. Deploy the functions from the `function` folder: `npm run deploy`

# Main dependencies

This project uses

- [kraken-api](https://github.com/nothingisdead/npm-kraken-api)
