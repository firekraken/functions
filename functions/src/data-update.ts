// eslint-disable-next-line no-unused-vars
import { QueryDocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
// eslint-disable-next-line no-unused-vars
import { Asset } from './kraken/snapshot';

export const updateData = async (db: FirebaseFirestore.Firestore, snap: QueryDocumentSnapshot) => {
    const { assets, total, timestamp } = snap.data();

    updateCurrent(db, total, timestamp);
    updateAth(db, total, timestamp);
    updateShares(db, assets);
    updateAssets(db, assets, timestamp);
};

const updateCurrent = async (db: FirebaseFirestore.Firestore, total: number, timestamp: any) => {
    await db.collection('data').doc('current').set({ timestamp, total });
};

const updateAth = async (db: FirebaseFirestore.Firestore, total: number, timestamp: any) => {
    const athRef = db.collection('data').doc('ath');
    const ath = await athRef.get();

    if (!ath.exists || (ath.data() as any).total < total) {
        await athRef.set({ timestamp, total });
    }
};

const updateShares = async (db: FirebaseFirestore.Firestore, assets: Asset[]) => {
    const valueRef = db.collection('data').doc('valueShares');
    const quantityRef = db.collection('data').doc('quantityShares');

    let values: { code: string; value: number }[] = [];
    let quantities: { code: string; quantity: number }[] = [];

    assets.forEach((asset) => {
        quantities.push({ code: asset.code, quantity: +asset.quantity.toFixed(2) });
        values.push({ code: asset.code, value: +(asset.quantity * asset.value).toFixed(2) });
    });

    values = values.filter((asset) => asset.value > 5);
    quantities = quantities.filter((asset) => values.findIndex((val) => val.code === asset.code) >= 0);

    await valueRef.set({ shares: values });
    await quantityRef.set({ shares: quantities });
};

const updateAssets = async (db: FirebaseFirestore.Firestore, assets: Asset[], timestamp: any) => {
    const assetsRef = db.collection('data').doc('assetValues');

    const currentAssets: { ath: number, current: number, key: string, timestamp: any }[] = ((await assetsRef.get()).data() as any).assets;

    const updatedAssets = assets.map((a) => {
        const current = +(a.quantity * a.value).toFixed(2);

        const asset = currentAssets.find(ca => ca.key === a.code) || {ath: current, current, key: a.code, timestamp};
        asset.current = current;
        if (current > asset.ath) {
            asset.ath = current;
            asset.timestamp = timestamp;
        }

        return asset;
    })

    await assetsRef.set({ assets: updatedAssets });
};
