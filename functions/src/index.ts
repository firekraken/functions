import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import Kraken from './kraken/kraken';
import { updateData } from './data-update';

admin.initializeApp();

exports.scheduledUpdate = functions
    .region('europe-west3')
    .pubsub.schedule('*/30 * * * *')
    .onRun(() => new Kraken().updateChart());

const db = admin.firestore();

exports.updateData = functions
    .region('europe-west3')
    .firestore.document('snapshots/{snapId}')
    .onCreate((snap) => updateData(db, snap));
