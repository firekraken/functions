export default interface Snapshot {
    timestamp: Date;
    assets: Asset[];
    total: number;
}

export interface Asset {
    code: string;
    value: number;
    quantity: number;
}
