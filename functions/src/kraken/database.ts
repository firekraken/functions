import {firestore} from 'firebase-admin';
// eslint-disable-next-line no-unused-vars
import Snapshot from './snapshot';

export default class Database {
    private readonly db = {
      snapshots: this.dataPoint<Snapshot>('snapshots'),
      logs: this.dataPoint<{timestamp: Date, log: string, stack: string}>('logs'),
    };

    async snapshots(): Promise<Snapshot[]> {
      const documents = await this.db.snapshots.get();
      const snapshots: Snapshot[] = [];

      documents.forEach((d) => snapshots.push(d.data()));

      return snapshots;
    }

    async insertSnapshot(snapshot: Snapshot) {
      await this.db.snapshots
          .doc(snapshot.timestamp.toISOString())
          .create(snapshot);
    }

    async insertLog(e: Error) {
      const now = new Date();
      await this.db.logs
          .doc(now.toISOString())
          .create({
            timestamp: now,
            log: e.message,
            stack: e.stack?.toString() ?? ''
          });
    }

    private dataPoint<T>(collectionPath: string) {
      return firestore().collection(collectionPath).withConverter(this.converter<T>());
    }

    private converter<T>() {
      return {
        toFirestore: (data: Partial<T>) => data,
        fromFirestore: (snap: FirebaseFirestore.QueryDocumentSnapshot) => snap.data() as T,
      };
    }
}
