const KrakenClient = require('kraken-api');
import * as functions from 'firebase-functions';
import Database from './database';
// eslint-disable-next-line no-unused-vars
import Snapshot from './snapshot';

type Balance = { [assets: string]: number };
type AssetToCode = { [asset: string]: string };
type AssetToValue = { [asset: string]: number };

export default class Kraken {
    private instance = new KrakenClient(functions.config().kraken.key, functions.config().kraken.secret);
    private readonly db = new Database();

    async updateChart() {
        try {
            const snapshot: Snapshot = {
                timestamp: new Date(),
                assets: [],
                total: 0
            };
            const balance = await this.balance();
            const assets = Object.keys(balance);
            const assetsToCodes = await this.assetCodes(assets);

            const pairs = await this.assetPairs(Object.values(assetsToCodes));

            Object.entries<string>(assetsToCodes).forEach(([asset, code]) => {
                const quantity = balance[asset];
                const value = pairs[asset];
                snapshot.assets.push({
                    code,
                    quantity,
                    value
                });
                snapshot.total += (quantity * value);
            });

            await this.db.insertSnapshot(snapshot);
        } catch (e) {
            await this.db.insertLog(e);
        }
    }

    private async balance(): Promise<Balance> {
        const response = await this.instance.api('Balance');

        if (response.error && response.error.length) {
            Promise.reject(`Balance : ${response.error}`);
        }

        return Object.entries<string>(response.result).reduce((acc: any, [key, value]) => {
            if (key.endsWith('EUR')) {
                return acc;
            }

            const sanitizedKey = key.endsWith('.S') ? key.substring(0, key.length - 2) : key;
            if (acc[sanitizedKey] != null) {
                acc[sanitizedKey] += parseFloat(value);
            } else {
                acc[sanitizedKey] = parseFloat(value);
            }

            return acc;
        }, {});
    }

    private async assetCodes(assets: string[]): Promise<AssetToCode> {
        const response = await this.instance.api('Assets', { asset: assets.join(',') });

        if (response.error && response.error.length) {
            Promise.reject(`Assets : ${response.error}`);
        }

        return assets.reduce((acc: any, current) => {
            acc[current] = response.result[current].altname

            return acc;
        }, {});
    }

    private async assetPairs(assets: string[]): Promise<AssetToValue> {
        const pair = assets.map(asset => `${asset}EUR`).join(',');
        const response = await this.instance.api('Ticker', { pair });

        if (response.error && response.error.length) {
            Promise.reject(`Ticker : ${response.error}`);
        }

        return Object.entries<any>(response.result)
            .reduce((acc: any, [key, value]) => {
                const name = key.includes('ZEUR')
                ? key.slice(0, -4)
                : key.slice(0, -3);
                acc[name] = +value.c[0];

                return acc;
            }, {});
    }
}
